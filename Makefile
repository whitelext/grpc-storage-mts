auth:
	protoc --go_out=./protofiles/auth ./protofiles/auth/auth.proto
	protoc --go-grpc_out=./protofiles/auth ./protofiles/auth/auth.proto
email:
	protoc --go_out=./protofiles/email ./protofiles/email/email.proto
	protoc --go-grpc_out=./protofiles/email ./protofiles/email/email.proto